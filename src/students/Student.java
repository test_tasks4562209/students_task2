package students;
import java.io.Serializable;
import java.util.*;

/**
 * Класс представляет студента с его основными характеристиками и списком предметов, которые он изучает.
 */
public class Student implements Serializable {
    private int id;
    private String firstName;
    private String lastName;
    private int age;
    private final List<Subject> subjects = new ArrayList<>();

    /**
     * Конструктор для создания экземпляра студента с указанными характеристиками.
     *
     * @param id        Идентификатор студента.
     * @param firstName Имя студента.
     * @param lastName  Фамилия студента.
     * @param age       Возраст студента.
     */
    public Student(int id, String firstName, String lastName, int age)
    {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;

    }

    /**
     * Получить идентификатор студента.
     *
     * @return Идентификатор студента.
     */
    public int getId() {
        return id;
    }

    /**
     * Установить идентификатор студента.
     *
     * @param id Новый идентификатор студента.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Получить имя студента.
     *
     * @return Имя студента.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Установить имя студента.
     *
     * @param firstName Новое имя студента.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Получить фамилию студента.
     *
     * @return Фамилия студента.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Установить фамилию студента.
     *
     * @param lastName Новая фамилия студента.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Получить возраст студента.
     *
     * @return Возраст студента.
     */
    public int getAge() {
        return age;
    }

    /**
     * Установить возраст студента.
     *
     * @param age Новый возраст студента.
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * Находит предмет с указанным именем в списке предметов студента.
     *
     * @param ref Имя предмета для поиска.
     * @return Экземпляр предмета или null, если предмет не найден.
     */
    public Subject findSubject(String ref)
    {
        for (Subject temp: subjects)
        {
            if (temp.getName().equals(ref))
            {
                return temp;
            }
        }
        return null;
    }

    /**
     * Добавляет предмет в список предметов студента.
     *
     * @param s Экземпляр предмета для добавления.
     */
    public void addSubject(Subject s) {
        subjects.add(s);
    }

    /**
     * Удаляет предмет из списка предметов студента.
     *
     * @param s Экземпляр предмета для удаления.
     */
    public void removeSubject(Subject s) {
        for (Subject subject : subjects) {
            if (subject.getName().equals(s.getName())) {
                subjects.remove(subject);
                break;
            }
        }

    }

    /**
     * Проверяет, содержит ли студент предмет с указанным именем.
     *
     * @param s Имя предмета для проверки.
     * @return true, если студент изучает предмет, в противном случае - false.
     */
    public boolean hasSubject(String s)
    {
        Subject st = findSubject(s);
        return st != null;
    }


    /**
     * Получает строковое представление всех предметов, изучаемых студентом.
     *
     * @return Строковое представление предметов студента.
     */
    public String getSubjects() {

        int indx =0;
        String s = "";
        while (indx < subjects.size())
        {
            Subject temp = subjects.get(indx);
            s = s + temp.getName() + " ";
            indx = indx+1;
        }
        return s;
    }

    /**
     * Возвращает строковое представление информации о студенте, включая уникальный идентификатор, имя, фамилию, возраст и список предметов, которые он изучает.
     *
     * @return Строковое представление студента.
     */
    public String toString() {
        return "Студент ID: "+ id + " -И.Ф: " +  firstName + " " +  lastName  + " Возраст: " +age + " -Предметы: " + getSubjects() + "\n";
    }
}
