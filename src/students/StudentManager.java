package students;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.util.*;

/**
 * Класс управляет списком студентов, сохранением и восстановлением данных.
 */
public class StudentManager implements Serializable{

    private final List<Student> students = new ArrayList<>();

    private String school;

    private String manager;

    /**
     * Конструктор для создания экземпляра менеджера студентов с указанным названием школы.
     *
     * @param school Название школы.
     */
    public StudentManager(String school) {

        this.school = school;
        manager = "";

    }

    /**

     * Получить название школы.
     *
     * @return Название школы.
     */public String getSchool() {
        return school;
    }

    /**
     * Установить название школы.
     *
     * @param school Новое название школы.
     */
    public void setSchool(String school) {
        this.school = school;
    }

    /**
     * Получить информацию о менеджере.
     *
     * @return Информация о менеджере.
     */
    public String getManager() {
        return manager;
    }

    /**
     * Установить информацию о менеджере.
     *
     * @param m Новая информация о менеджере.
     */
    public void setManager(String m)
    {
        manager =m;
    }

    /**
     * Получить студента по его идентификатору.
     *
     * @param std Идентификатор студента.
     * @return Экземпляр студента или null, если студент не найден.
     */
    public Student getStudent(int std)
    {
        for (Student temp: students)
        {
            if (temp.getId() ==  std)
            {
                return temp;
            }
        }
        return null;
    }
    /**
     * Найти студента по имени.
     *
     * @param ref Имя студента.
     * @return Экземпляр студента или null, если студент не найден.
     */
    public Student findStudent(String ref)
    {
        for (Student temp: students)
        {
            if (temp.getFirstName().equals(ref))
            {
                return temp;
            }
        }
        return null;
    }

    /**
     * Добавить студента в список.
     *
     * @param s Экземпляр студента.
     */
    public void addStudent(Student s) {
        students.add(s);

    }

    /**
     * Удалить студента из списка.
     *
     * @param s Экземпляр студента.
     */
    public void removeStudent(Student s) {

        for (Student student : students) {
            if (student.getFirstName().equals(s.getFirstName())) {
                students.remove(student);
                break;
            }
        }

    }

    /**
     * Проверить, есть ли студент с указанным именем в списке.
     *
     * @param s Имя студента.
     * @return true, если студент найден, в противном случае - false.
     */
    public boolean hasStudent(String s)
    {
        Student sd = findStudent(s);
        return sd != null;
    }


    /**
     * Получить строковое представление всех студентов в списке.
     *
     * @return Строковое представление всех студентов.
     */
    public String getAllStudents() {
        String s = " List of Students: " + "\n";
        for (Student sd : students)
        {
            s = s + sd.toString();

        }
        return s;

    }

    /**
     * Поиск студентов по указанному предмету.
     *
     * @param subject Предмет для поиска.
     * @return Строковое представление студентов, изучающих указанный предмет.
     */
    public String searchBySubject(String subject) {
        String s= "Students studying " + subject + "\n";
        for (Student student : students) {
            if (student.getSubjects().contains(subject)) {
                s = s + student;
            }
        }
        return s;
    }


    /**
     * Сохранить список студентов в файл.
     *
     * @param filename Имя файла для сохранения данных.
     */
    public void saveToFile(String filename)
    {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try(FileWriter writer = new FileWriter(filename)) {
            gson.toJson(this,writer);
            System.out.println("Список студентов сохранен в " + filename + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Восстановить список студентов из файла.
     *
     * @param filename Имя файла для восстановления данных.
     * @return Восстановленный экземпляр менеджера студентов.
     */
    public StudentManager restoreManager(String filename) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try (FileReader reader = new FileReader(filename)) {


            StudentManager deserializedManager = gson.fromJson(reader, StudentManager.class);
            System.out.println("Список студентов загружен из " + filename + "\n");
            return deserializedManager;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Возвращает строковое представление информации о менеджере студентов, включая школу и список студентов.
     *
     * @return Строковое представление менеджера.
     */
    public String toString() {

        return "Менеджер " + manager + " школы " + school + "\n" + getAllStudents() +"\n";

    }
}
