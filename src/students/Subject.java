package students;

import java.io.Serializable;

/**
 * Класс представляет предмет, который может изучать студент.
 */
public class Subject implements Serializable {

    private String name;

    /**
     * Конструктор для создания экземпляра предмета с указанным именем.
     *
     * @param name Имя предмета.
     */
    public Subject(String name) {
        this.name = name;
    }

    /**
     * Получить имя предмета.
     *
     * @return Имя предмета.
     */
    public String getName() {
        return name;
    }

    /**
     * Установить имя предмета.
     *
     * @param name Новое имя предмета.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Возвращает строковое представление информации о предмете, включая название предмета.
     *
     * @return Строковое представление предмета.
     */
    public String toString() {
        return "Название: " + name;
    }
}

