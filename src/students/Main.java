package students;

/**
 * Основной класс для запуска программы.
 *
 */
public class Main {

    /**
     * Точка входа в программу.
     *
     * @param args Аргументы командной строки.
     */
    public static void main(String[] args) {


        StudentManager studentManager = new StudentManager("Hogwarts");

        studentManager.addStudent(new Student(100, "John", "Perry", 12));
        studentManager.addStudent(new Student(101, "Maya", "Rudolph", 14));

        System.out.println(studentManager.getAllStudents());
        
        studentManager.getStudent(100).addSubject(new Subject("Math"));
        studentManager.getStudent(101).addSubject(new Subject("Physics"));
        studentManager.getStudent(101).addSubject(new Subject("Chemistry"));
        studentManager.getStudent(100).addSubject(new Subject("Chemistry"));
        
        System.out.println(studentManager.getAllStudents());


        System.out.println(studentManager);
        System.out.println("Студентыб изучающие Химию: "+ studentManager.searchBySubject("Chemistry"));
        
        studentManager.saveToFile("studentsmanager.json");
        StudentManager readmanager = studentManager.restoreManager("studentsmanager.json");
        System.out.println(readmanager.toString());


















    }
}





